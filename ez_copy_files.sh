#!/bin/bash
##############################################################################################################################
##############################################################################################################################
#### Author: Mauricio Garcia                                                                                              ####
#### Company: Nutanix.                                                                                                    ####
#### Mail: mauricio.garcia@nutanix.com                                                                                    ####
#### Description: Base on the user input #number case, creates a folder into the ˜/shared/${case#} folder,                ####
####              copy the files found in /mnt/data_vol/${case#} and unzip all the files in the                           ####
####             destination ˜/shared/$####{case#} folder.                                                                ####
#### Date: Nov-5th-2019                                                                                                   ####
#### Modified: No1-2019                                                                                                   ####
#### Version: 3.0                                                                                                         ####
#### Script: ez_copy_diamond.sh                                                                                           ####
#### Location: ~/  (user home directory into the Diamond-Panacea server.                                                  ####
#### Instructions:  Mandatory: The user needs to type the case number without zeros and hit enter.                        ####
####                the files will be copied and unzip from /mnt/data_vol/${case#} into a new destination folder folder   ####
####                located in /home/${user-id}/shared/${case#} folder                                                    ####
####                I.E: ./ez_copy_diamond.sh   (hit enter to continue)                                                   ####
####                      Please insert the case number in format , i.e: 999999:   (hit enter to insert the case)         ####
####                      111111         (type the case number and hit enter)                                             ####
####                      Files will be copied and unzipped in new folder destination /home/${user}/shared/111111         ####
#### Changes: 1. Menu added with options to select specific folders to be collected. 2. now using functions to call       ####
###           call instructions. 
###           Aug-25-2020: Fixed Logbay multiple .zip files removed before unzip. Cleaning Menu, dynamic variables added  ####
##############################################################################################################################
##############################################################################################################################

HOME="/users/`id|cut -d"(" -f2|cut -d")" -f1`"
SHOME="${HOME}/shared"
STD='\033[0;0;39m'
RED='\033[0;41;30m'
var1="NOVALUE" 
var2="NOVALUE"
var3="NOVALUE"
var4="NOVALUE"
var5="NOVALUE"
var6="NOVALUE"

usage()
{
    echo "usage: [-h | --help ] to show this menu"
    echo "Value must be an integer number between 000001 and 999999"
    echo -e "i.e. $0 press [ENTER] and type the case number in the next line \n123456 \n"
}

while [ "$1" != "" ]; do
   case $1 in
        -h | --help )           usage
                                exit
                                ;;
         '' )                   usage
                                exit
                                ;;
        * )                     usage
                                exit 1
   esac
shift  
done

###Asking for the case number and save it in variable case
echo -n "Please insert the case number in format , i.e: 999999 and press [ENTER]: "
read case

###Check for input if none show help.

if [[ ${#case} -ne 6 ]] || [[ $case == *[!0-9]* ]] || [[ -z "${case}" ]]
   then 
    echo "No passed. Value is not numeric, or is empty or length is not equals to 6. Please try again!!! " 
    usage
    exit 1
else 
   echo "Value is valid and equals to $case"
fi
if [ -d /mnt/data_vol/${case} ] 
   then
    dates=`ls -ltr /mnt/data_vol/${case} | grep ^d | wc -l`
    
    if [[ ${dates} -gt 0 ]] && [[ ${dates} -le 10 ]] 
       then 
       counter=1
       while (($counter <= $dates))
       do
       eval "var${counter}"="'$(ls -lrt /mnt/data_vol/${case}/ | grep ^d | awk '{print $9}' | tail -${counter} | head -1)'"
       let counter++
       done
    else 
         echo "Folder is empty or files cannot be copied from /mnt/data_vol/${case} to ~/shared/${case} "
    fi
else
   echo "The directory /mnt/data_vol/${case} does not exist or cannot be accessed...bye" 
   exit
fi

date1 () {
  echo "------Your choose was 1 and will copy files for ${var1} -----------"
  ndate=${var1} 
  funcopy
}
date2 () {
  echo "------Your choose was 2 and will copy files for ${var2} -----------"
  if [[ "${var2}" != "NOVALUE" ]] 
  then
     ndate=${var2} 
     funcopy
  else
     echo "The folder is empty, try another option"
  fi
}
date3 () {
  echo "------Your choose was 3 and will copy files for ${var3} -----------"
  if [[ "${var3}" != "NOVALUE" ]] 
  then
     ndate=${var3} 
     funcopy
  else
     echo "The folder is empty, try another option"
  fi
}
date4 () {
  echo "------Your choose was 4 and will copy files for ${var4} -----------"
  if [[ "${var4}" != "NOVALUE" ]] 
  then
     ndate=${var4} 
     funcopy
  else
     echo "The folder is empty, try another option"
  fi
}
date5 () {
  echo "------Your choose was 5 and will copy files for ${var5} -----------"
  if [[ "${var5}" != "NOVALUE" ]] 
  then
     ndate=${var5} 
     funcopy
  else
     echo "The folder is empty, try another option"
  fi
}
date6 () {
  echo "------Your choose was 6 and will copy files for ${var6} -----------"
  if [[ "${var6}" != "NOVALUE" ]] 
  then
     ndate=${var6} 
     funcopy
  else
     echo "The folder is empty, try another option"
  fi
}
alldates () {
  echo "------Your choose was ALL and we will copy all the files  -----------"
  var1=`ls -lrt /mnt/data_vol/${case}/ | grep ^d | awk '{print $9}' | wc -l` 
  if [[ "${var1}" != "NOVALUE" ]] 
  then
     for ndate in `ls -lrt /mnt/data_vol/${case}/ | grep ^d | awk '{print $9}'` 
         do
         funcopy
     done
  else
     echo "The folder is empty, try another option"
  fi
}

funcopy () {
     ndir="${SHOME}/${case}/${ndate}"
     mkdir -p ${ndir} 
     cd ${ndir}
     cp -r /mnt/data_vol/${case}/${ndate}/*.* ${ndir}/
     echo "Uncompressing files, please be patient, this routine might take more than 5 minutes"
     for file in `ls -1 ${ndir}/ | egrep -i ".tar.gz"`
      do
         cd ${ndir}
         tar -xzf ${ndir}/${file} && rm -r ${ndir}/${file} || echo "The file ${file} was not uncompressed or removed in ${ndir}"
     done
     for zfile in `ls -1 ${ndir} | egrep -v "\@|\*|\/" | grep ".zip"`
      do
         cd ${ndir}
         unzip -q -o ${ndir}/${zfile} > /dev/null 2>&1 
     done
     for nzfile in `ls -1 ${ndir}/*-PE | egrep -v "\@|\*|\/" | grep ".zip"`
      do
         cd ${ndir}/*-PE
         unzip -q -o ${ndir}/*-PE/${nzfile} > /dev/null 2>&1 
     done
     find ${ndir}/*-PE -type f -iname "*.tar.gz" \( -exec \tar -xzf {} \; -exec rm {} \; \) > /dev/null 2>&1 & 
     find ${ndir} -type f -iname "*.zip" -exec \rm {} \; > /dev/null 2>&1
     echo "------------------------------------------------------------------------------------------"
             echo -e " Files has been copied and uncompress in dest folder ${ndir} \n"       
     ls -lrt ${ndir}
}

show_menus() {
        clear
        echo "~~~~~~~~~~~~~~~~~~~~~"
        echo " The case # ${case} is valid, now select one option for logs collection"
        echo "~~~~~~~~~~~~~~~~~~~~~"
        counter=1
        while (($counter <= $dates))
        do
        mydate=$(echo -e "\$var$counter")
        echo "${counter}. Hit number \"${counter}\" for subdirectory `eval echo ${mydate}` and press [ENTER]: "
        let counter++
        done
        echo "---> Type the word  \"all\" to collect ALL the files available for /mnt/data_vol/${case} folder and press [ENTER]: " 
        echo "---> Type the word \"exit\" to terminate this script"
}

show_menus
read option
case ${option} in
     1) date1 ;;
     2) date2 ;;
     3) date3 ;;
     4) date4 ;;
     5) date5 ;;
     6) date6 ;;
     all) alldates ;;
     exit) exit 0;;
     *) echo -e "${RED}Error, option is not valid, try again ... ${STD}" 
esac